$(document).ready(function(){
    $("#execute").click(function(){
        var text = $("#query").val();
        console.log(text);
        $.post( "http://calmisko.org:7474/db/data/cypher", { query: text}, function() {
          console.log( "success" );
        })
          .done(function(data) {
            console.log( "Dades" + data.data + "ciao");
            console.log(data.data.length);
            var objJson={};
            var str={};
            str.name="flare"
            str.children=new Array();
            str.children[0]=new Object();
            str.children[0].name="data"
            str.children[0].children=new Array();   
            for(var i=0; i < data.data.length; i++) {
                str.children[0].children[i]={};
                if (data.data[i][0].data.name != undefined) { 
                    //for query 4
                    str.children[0].children[i].name=data.data[i][0].data.name;
                }
                else if (data.data[i][0].data.value != undefined){
                    //for query 1
                    str.children[0].children[i].name=data.data[i][0].data.value;
                }
                else {
                    str.children[0].children[i].name="empty";
                }
                str.children[0].children[i].size=data.data[i][1];
                
                
                
                
            }
            objJson=JSON.stringify(str);
            console.log(objJson);
            
            $.getScript('http://d3js.org/d3.v3.min.js', function(){
            var diameter = 960,
            format = d3.format(",d"),
            color = d3.scale.category20c();
           
            var bubble = d3.layout.pack()
                .sort(null)
                .size([diameter, diameter])
                .padding(1.5);
    
            var svg = d3.select("#chart").append("svg")
                .attr("width", diameter)
                .attr("height", diameter)
                .attr("class", "bubble");
    
            root=JSON.parse(objJson);
              var node = svg.selectAll(".node")
                  .data(bubble.nodes(classes(root))
                  .filter(function(d) { return !d.children; }))
                .enter().append("g")
                  .attr("class", "node")
                  .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
    
              node.append("title")
                  .text(function(d) { return d.className + ": " + format(d.value); });
    
              node.append("circle")
                  .attr("r", function(d) { return d.r; })
                  .style("fill", function(d) { return color(d.packageName); });
    
              node.append("text")
                  .attr("dy", ".3em")
                  .style("text-anchor", "middle")
                  .text(function(d) { return d.className.substring(0, d.r / 3); });
            
    
            // Returns a flattened hierarchy containing all leaf nodes under the root.
            function classes(root) {
              var classes = [];
    
              function recurse(name, node) {
                if (node.children) 
                    node.children.forEach(function(child) { recurse(node.name, child); });
                else 
                    classes.push({packageName: name, className: node.name, value: node.size});
              }
    
              recurse(null, root);
              return {children: classes};
            }
    
            d3.select(self.frameElement).style("height", diameter + "px");
            })
          
          })
          .fail(function() {
            alert( "error" );
          })
          .always(function() {
            console.log( "finished" );
        });
    });
});

